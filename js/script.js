"use strict";
//1.forEach перебирає масив та викликає функцію до кожного елемента масиву. Він нічого не повертає,
//тільки перебирає.
//2.Можна виставити length на 0 (тобто someArr.length = 0), або методом splice(someArr.splice(0,someArr.length).
//3.Найбільш надійно - Array.isArray(someArr) - розпізнає null. Можливе застосування
//instanceof Array а також Object.prototype.toString() - схоже на Array.isArray().


let array = ['hello', 'world', 23, '23', null, false, true, null, 12];

const filterBy = function (arr, type){
   // console.log("filterBy is running for type: ", type)
   if(Array.isArray(array)){
   array.forEach(item => {
      if (typeof item !== type) {
         console.log(item);
      }
   });
}else{
   console.log ("Not an array");
   }
}
// const allTypes = ['string', 'number', 'boolean'];
// allTypes.forEach(type => filterBy(array, type));
filterBy(array,'string');

//Рішення другим методом filter.

// let array = ['hello', 'world', 23, '23', null];
//
// const filterBy = function (arr, type){
//    if(Array.isArray(array)){
//       return array.filter(item => typeof item !== type);
//    }else{
//       return "Not an array";
//    }
// }
// console.log(filterBy(array,'string'));
